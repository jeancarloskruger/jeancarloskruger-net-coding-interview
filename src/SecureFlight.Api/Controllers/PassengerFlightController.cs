﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureFlight.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PassengerFlightController : ControllerBase
    {
        private readonly IService<PassengerFlight> _passengerFlightService;
        private readonly IRepository<PassengerFlight> _passengerFlightRepository;
        private readonly IRepository<Passenger> _passengerRepository;
        private readonly IRepository<Flight> _flightFlightRepository;

        public PassengerFlightController(IRepository<PassengerFlight> passengerFlightRepository, IRepository<Passenger> passengerRepository, IRepository<Flight> flightFlightRepository)
        {
            _passengerFlightRepository = passengerFlightRepository;
            _passengerRepository = passengerRepository;
            _flightFlightRepository = flightFlightRepository;
        }

        [HttpPut]
        [ProducesResponseType(typeof(AirportDataTransferObject), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public IActionResult Put(string passengerId, long? flightId)
        {
            if (string.IsNullOrWhiteSpace(passengerId))
            {
                return BadRequest("Missing passengerId");
            }


            if (!flightId.HasValue)
            {
                return BadRequest("Missing flightId");
            }

            //var passenger = _passengerRepository.GetById(passengerId);
            // var flight = _flightFlightRepository.GetById(flightId);

            var result = _passengerFlightRepository.Add(new PassengerFlight
            {
                FlightId = flightId.Value,
                PassengerId = passengerId
            });
            return Ok(result);
        }
    }
}
