﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecureFlight.Core.Services
{
    public class PassengerFlightService : BaseService<PassengerFlight>
    {

        public PassengerFlightService(IRepository<PassengerFlight> repository) : base(repository)
        {
        }

    }
}
